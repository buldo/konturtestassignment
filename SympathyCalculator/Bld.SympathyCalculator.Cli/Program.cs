﻿using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace Bld.SympathyCalculator.Cli
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Common;

    class Program
    {
        private const uint NAMES_FILES_POSITION = 0;
        private const uint SYMP_FILES_POSITION = 1;
        private const int DEFAULT_FAV_COUNT = 3;

        private static readonly Dictionary<string, Action> _commands = new Dictionary<string, Action>()
        {
            {"/nf", CalculateAndPrintNf},
            {"/fv", CalculateAndPrintFv},
            {"/uh", CalculateAndPrintUh}
        };

        private static readonly DataLoader _loader = new DataLoader();

        private static ICalculator _calculator;

        static void Main(string[] args)
        {
            var container = new UnityContainer();
            container.LoadConfiguration();
            _calculator = new CalculatorCache(container.Resolve<ICalculator>());

            if (args.Length < 3)
            {
                PrintHelp();
            }

            var namesPath = args[NAMES_FILES_POSITION];
            if (!File.Exists(namesPath))
            {
                Console.WriteLine("Файл имён не существует");
                PrintHelp();
            }

            var sympPath = args[SYMP_FILES_POSITION];
            if (!File.Exists(sympPath))
            {
                Console.WriteLine("Файл симпатий не существует");
                PrintHelp();
            }

            var userCommand = args.Skip(2).ToList();
            foreach (var com in userCommand)
            {
                if (!_commands.ContainsKey(com))
                {
                    Console.WriteLine($"Неизвестный аргумент {com}");
                    PrintHelp();
                    return;
                }
            }

            List<string> names;
            List<Tuple<string, string>> sympathies;
            _loader.LoadData(namesPath, sympPath, out names, out sympathies);
            _calculator.Load(names, sympathies);

            foreach (var com in userCommand)
            {
                _commands[com].Invoke();
            }

            Console.ReadLine();
        }

        private static void PrintHelp()
        {
            Console.WriteLine("Формат параметров: ФАЙЛ_ИМЁН ФАЙЛ_СВЯЗЕЙ [/nf /fv /uh]");
            Console.WriteLine("/nf список всех не любимчиков");
            Console.WriteLine("/fv список любимчиков");
            Console.WriteLine("/uh список всех несчастных детей");
        }

        private static void CalculateAndPrintNf()
        {
            Console.WriteLine("Cписок всех не любимчиков");
            PrintResultsInLine(_calculator.GetNonFavoriteChildren());
        }

        private static void CalculateAndPrintFv()
        {
            Console.WriteLine("Cписок любимчиков");
            PrintResultsInLine(_calculator.GetFavoriteChildren(DEFAULT_FAV_COUNT));
        }

        private static void CalculateAndPrintUh()
        {
            Console.WriteLine("Cписок всех несчастных детей");
            PrintResultsInLine(_calculator.GetUnhappyChildren());
        }

        private static void PrintResultsInLine(List<string> toPrint)
        {
            Console.WriteLine(string.Join(",", toPrint));
        }
    }
}
