﻿using System.Linq;

namespace Bld.SympathyCalculator.Oop
{
    using System;
    using System.Collections.Generic;
    using Common;

    public class OopCalculator : ICalculator
    {
        private HashSet<Person> _children;

        /// <summary>
        /// Загрузка списков детей и их симпатий
        /// </summary>
        /// <param name="children">Список детей</param>
        /// <param name="sympathy">Список симпатий</param>
        public void Load(List<string> children, List<Tuple<string, string>> sympathy)
        {
            _children = new HashSet<Person>(children.Select(name => new Person(name)));

            foreach (var sympPair in sympathy)
            {
                _children.First(ch => ch.Name == sympPair.Item1)
                    .Sympathies.Add(_children.First(ch => ch.Name == sympPair.Item2));
            }
        }

        /// <summary>
        /// Возвращает список всех не любимчиков
        /// </summary>
        /// <remarks>
        /// Не любимчики - дети, которые никому не симпатичны
        /// </remarks>
        /// <returns>Имена детей</returns>
        public List<string> GetNonFavoriteChildren()
        {
            return _children.Where(ch => _children.All(comp => comp.Sympathies.All(sym => sym != ch))).Select(o => o.Name).ToList();
        }

        /// <summary>
        /// Возвращает список всех несчастных детей
        /// </summary>
        /// <remarks>
        /// Несчастные дети - дети, которые не симпатичны ни одному ребенку из тех, кто симпатичен им самим. За исключением тех детей, которым никто не симпатичен.
        /// </remarks>
        /// <returns>Имена детей</returns>
        public List<string> GetUnhappyChildren()
        {
            return _children.Where(ch => ch.Sympathies.Count != 0)
                .Where(ch => ch.Sympathies.All(sym => !sym.Sympathies.Contains(ch)))
                .Select(o => o.Name)
                .ToList();
        }

        /// <summary>
        /// Возвращает список любимчиков
        /// </summary>
        /// <param name="count">
        /// Число любимчиков для поиска.
        /// </param>
        /// <remarks>
        /// Любимчики - детей, которые симпатичны максимальному количеству других детей.
        /// Если число запрошенных любимчиков больше либо равно числу детей, то будут выведены все дети по порядку убывания симпатий. За исключением числа не любимчиков.
        /// </remarks>
        /// <returns>Имена детей</returns>
        public List<string> GetFavoriteChildren(int count)
        {
            return
                _children.Select(ch => new {Children = ch, Count = _children.Count(tst => tst.Sympathies.Contains(ch))})
                    .OrderByDescending(o => o.Count)
                    .Take(count)
                    .Select(o => o.Children.Name)
                    .ToList();
        }
    }
}
