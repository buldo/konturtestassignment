﻿namespace Bld.SympathyCalculator.Common
{
    using System;
    using System.Collections.Generic;

    public interface ICalculator
    {
        /// <summary>
        /// Загрузка списков детей и их симпатий
        /// </summary>
        /// <param name="children">Список детей</param>
        /// <param name="sympathy">Список симпатий</param>
        void Load(List<string> children, List<Tuple<string, string>> sympathy);

        /// <summary>
        /// Возвращает список всех не любимчиков
        /// </summary>
        /// <remarks>
        /// Не любимчики - дети, которые никому не симпатичны
        /// </remarks>
        /// <returns>Имена детей</returns>
        List<string> GetNonFavoriteChildren();

        /// <summary>
        /// Возвращает список всех несчастных детей
        /// </summary>
        /// <remarks>
        /// Несчастные дети - дети, которые не симпатичны ни одному ребенку из тех, кто симпатичен им самим. За исключением тех детей, которым никто не симпатичен.
        /// </remarks>
        /// <returns>Имена детей</returns>
        List<string> GetUnhappyChildren();

        /// <summary>
        /// Возвращает список любимчиков
        /// </summary>
        /// <param name="count">
        /// Число любимчиков для поиска.
        /// </param>
        /// <remarks>
        /// Любимчики - детей, которые симпатичны максимальному количеству других детей.
        /// Если число запрошенных любимчиков больше либо равно числу детей, то будут выведены все дети по порядку убывания симпатий. За исключением числа не любимчиков.
        /// </remarks>
        /// <returns>Имена детей</returns>
        List<string> GetFavoriteChildren(int count);
    }
}