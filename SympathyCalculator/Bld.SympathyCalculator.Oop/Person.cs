﻿namespace Bld.SympathyCalculator.Oop
{
    using System.Collections.Generic;

    /// <summary>
    /// Человек
    /// </summary>
    internal class Person
    {
        public Person(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Получает имя человека
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Получает список симпатий человека
        /// </summary>
        public HashSet<Person> Sympathies { get; } = new HashSet<Person>();
    }
}
