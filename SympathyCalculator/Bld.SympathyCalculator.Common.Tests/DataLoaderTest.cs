namespace Bld.SympathyCalculator.Common.Tests
{
    using System;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    
    [TestClass]
    public class DataLoaderTest
    {
        [TestMethod]
        public void LoadData()
        {
            var chPath = "TestData/Children.txt";
            var sympPatch = "TestData/Sympathies.txt";

            var loader = new DataLoader();
            List<string> names;
            List<Tuple<string, string>> symp;
            loader.LoadData(chPath, sympPatch, out names, out symp);
            Assert.AreEqual(5, names.Count);
            CollectionAssert.AllItemsAreUnique(names);
            CollectionAssert.AreEquivalent(new[] {"����", "����", "����", "�����", "����"}, names);

            Assert.AreEqual(6, symp.Count);
            CollectionAssert.AllItemsAreUnique(symp);
            CollectionAssert.AreEquivalent(new[]
            {
                new Tuple<string, string>("����", "����"),
                new Tuple<string, string>("����", "����"),
                new Tuple<string, string>("�����", "����"),
                new Tuple<string, string>("�����", "����"),
                new Tuple<string, string>("����", "����"),
                new Tuple<string, string>("����", "�����")
            }, symp);
        }
    }
}
