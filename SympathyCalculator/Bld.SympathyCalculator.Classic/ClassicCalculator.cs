﻿namespace Bld.SympathyCalculator.Classic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Common;

    public class ClassicCalculator : ICalculator
    {
        /// <summary>
        /// Соответствия между именами и индексами
        /// </summary>
        private Dictionary<string, int> _indexes;
        private Dictionary<int, string> _reverseIndexes;

        private int _childrenCount;

        /// <summary>
        /// Матрица симпатий.
        /// Первый индекс - кому нравися. Второй - кто нравится.
        /// </summary>
        private bool[,] _connections;

        /// <summary>
        /// Загрузка списков детей и их симпатий
        /// </summary>
        /// <param name="children">Список детей</param>
        /// <param name="sympathy">Список симпатий</param>
        public void Load(List<string> children, List<Tuple<string, string>> sympathy)
        {
            _childrenCount = children.Count; // Думаю, семантически верно хранить число детей в отдельной переменной

            // Составляем индекс имён
            _indexes = new Dictionary<string, int>(_childrenCount);
            _reverseIndexes = new Dictionary<int, string>(_childrenCount);
            for (int i = 0; i < _childrenCount; i++)
            {
                var name = children[i];
                _indexes.Add(name, i);
                _reverseIndexes.Add(i, name);
            }

            // Составляем матрицу связей
            _connections = new bool[_childrenCount, _childrenCount];
            for (int i = 0; i < sympathy.Count; i++)
            {
                var pair = sympathy[i];
                _connections[_indexes[pair.Item1], _indexes[pair.Item2]] = true;
            }
        }

        /// <summary>
        /// Возвращает список всех не любимчиков
        /// </summary>
        /// <remarks>
        /// Не любимчики - дети, которые никому не симпатичны
        /// </remarks>
        /// <returns>Имена детей</returns>
        public List<string> GetNonFavoriteChildren()
        {
            var unFav = new List<string>();

            // При данном формате хранения, если в столбце все значения false, то ребёнок никому не симпатичен
            for (int i = 0; i < _childrenCount; i++) // Проходим по всем детям
            {
                var haveLikes = false;
                for (int j = 0; j < _childrenCount; j++) // Проверяем все возможные симпатии к ребёнку
                {
                    if (_connections[j, i])
                    {
                        haveLikes = true;
                        break;
                    }
                }

                if (!haveLikes)
                {
                    unFav.Add(_reverseIndexes[i]);
                }
            }

            return unFav;
        }

        /// <summary>
        /// Возвращает список всех несчастных детей
        /// </summary>
        /// <remarks>
        /// Несчастные дети - дети, которые не симпатичны ни одному ребенку из тех, кто симпатичен им самим. За исключением тех детей, которым никто не симпатичен.
        /// </remarks>
        /// <returns>Имена детей</returns>
        public List<string> GetUnhappyChildren()
        {
            var unhappy = new List<string>();

            for (int i = 0; i < _childrenCount; i++) // Проходим по всем детям
            {
                var haveSympathy = false;
                var haveMutualSympathy = false;
                for (int j = 0; j < _childrenCount; j++) // Проходимся по детям, которые симпатичны ребёнку-кандидату
                {
                    if (_connections[i, j]) // Проверяем, а симпатичен ли данный ребёнок кандидат тому, кто симпатичен ему
                    {
                        haveSympathy = true;
                        if (_connections[j, i])
                        {
                            haveMutualSympathy = true;
                            break;
                        }
                    }
                }

                if (!haveSympathy || haveMutualSympathy)
                {
                    // Есть взаимная симпатия или ему ни кто не нравится. Преходим к следующему.
                    continue;
                }

                // Взаимных симпатий нет. Заносим в список.
                unhappy.Add(_reverseIndexes[i]);
            }

            return unhappy;
        }

        /// <summary>
        /// Возвращает список любимчиков
        /// </summary>
        /// <param name="count">
        /// Число любимчиков для поиска.
        /// </param>
        /// <remarks>
        /// Любимчики - детеи, которые симпатичны максимальному количеству других детей.
        /// Если число запрошенных любимчиков больше либо равно числу детей, то будут выведены все дети по порядку убывания симпатий. За исключением числа не любимчиков.
        /// </remarks>
        /// <returns>Имена детей</returns>
        public List<string> GetFavoriteChildren(int count)
        {
            var sympCount = new List<Tuple<string,int>>(); // Список для хранения подсчитанных данных

            for (int i = 0; i < _childrenCount; i++)
            {
                int likes = 0;
                for (int j = 0; j < _childrenCount; j++)
                {
                    if (_connections[j, i])
                    {
                        likes += 1;
                    }
                }

                if (likes > 0)
                {
                    sympCount.Add(new Tuple<string, int>(_reverseIndexes[i], likes));
                }
            }

            // Используем немного LINQ. Отсортируем результаты и выберем необходимое число значений
            // Хотя можно было и использовать SortedList
            sympCount.Sort((first, second) => first.Item2.CompareTo(second.Item2));
            var startIndex = sympCount.Count - count;
            
            return startIndex >= 0 ? sympCount.GetRange(startIndex, count).Select(o => o.Item1).ToList() : sympCount.Select(o => o.Item1).ToList();
        }
    }
}
