﻿namespace Bld.SympathyCalculator.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.IO;
    
    public class DataLoader
    {
        public void LoadData(string namesPath, string sympPath, out List<string> names,
            out List<Tuple<string, string>> sympathies)
        {
            names = new List<string>();
            sympathies = new List<Tuple<string, string>>();

            names.AddRange(File.ReadLines(namesPath).Select(o => o.Trim()));

            foreach (var pair in File.ReadLines(sympPath))
            {
                var splited = pair.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                sympathies.Add(new Tuple<string, string>(splited[0], splited[1]));
            }
        }
    }
}
