﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bld.SympathyCalculator.Common.Tests
{
    public abstract class CalculatorTestBase
    {
        private readonly ICalculator _calculator;

        protected CalculatorTestBase(ICalculator realization)
        {
            _calculator = realization;
        }

        [TestInitialize]
        public void Initialize()
        {
            var chPath = "TestData/Children.txt";
            var sympPatch = "TestData/Sympathies.txt";

            var loader = new DataLoader();
            List<string> names;
            List<Tuple<string, string>> symp;
            loader.LoadData(chPath, sympPatch, out names, out symp);
            _calculator.Load(names, symp);
        }

        protected void TestNonFavoriteRealization()
        {
            var calcResult = _calculator.GetNonFavoriteChildren();
            CollectionAssert.AreEquivalent(new[] {"Саша", "Катя"}, calcResult);
        }

        protected void TestUnhappyRealization()
        {
            var calcResult = _calculator.GetUnhappyChildren();
            CollectionAssert.AreEquivalent(new[] {"Саша", "Катя"}, calcResult);
        }

        protected void TestFavoriteRealization()
        {
            var calcResult = _calculator.GetFavoriteChildren(1);
            CollectionAssert.AreEquivalent(new[] {"Стёпа"}, calcResult);
        }
    }
}
