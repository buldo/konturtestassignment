namespace Bld.SympathyCalculator.Oop.Tests
{
    using Common.Tests;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class CalculatorTest : CalculatorTestBase
    {
        public CalculatorTest()
            : base(new OopCalculator())
        {
        }

        [TestMethod]
        public void TestNonFavorite()
        {
            TestNonFavoriteRealization();
        }

        [TestMethod]
        public void TestUnhappy()
        {
            TestUnhappyRealization();
        }

        [TestMethod]
        public void TestFavorite()
        {
            TestFavoriteRealization();
        }
    }
}
