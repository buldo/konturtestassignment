﻿namespace Bld.SympathyCalculator.Common
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Класс-обёртка, умеющий кэшировать результаты вычислений
    /// </summary>
    public class CalculatorCache : ICalculator
    {
        /// <summary>
        /// Настоящий калькулятор, производящий рассчёты
        /// </summary>
        private readonly ICalculator _realCalculator;

        // Коллекции-кэши
        private List<string> _lastNonFavoriteChildrenResult;
        private List<string> _lastUnhappyChildrenResult;
        private List<string> _lastFavoriteChildrenResult;

        public CalculatorCache(ICalculator calculator)
        {
            _realCalculator = calculator;
        }

        /// <summary>
        /// Загрузка списков детей и их симпатий
        /// </summary>
        /// <param name="children">Список детей</param>
        /// <param name="sympathy">Список симпатий</param>
        public void Load(List<string> children, List<Tuple<string, string>> sympathy)
        {
            // Сброс кэшей
            _lastNonFavoriteChildrenResult = null;
            _lastUnhappyChildrenResult = null;
            _lastFavoriteChildrenResult = null;

            _realCalculator.Load(children, sympathy);
        }


        /// <summary>
        /// Возвращает список всех не любимчиков
        /// </summary>
        /// <remarks>
        /// Не любимчики - дети, которые никому не симпатичны
        /// </remarks>
        /// <returns>Имена детей</returns>
        public List<string> GetNonFavoriteChildren()
        {
            // ReSharper disable once ConvertIfStatementToNullCoalescingExpression Так понятнее
            if (_lastNonFavoriteChildrenResult == null)
            {
                _lastNonFavoriteChildrenResult = _realCalculator.GetNonFavoriteChildren();
            }

            return _lastNonFavoriteChildrenResult;
        }

        /// <summary>
        /// Возвращает список всех несчастных детей
        /// </summary>
        /// <remarks>
        /// Несчастные дети - дети, которые не симпатичны ни одному ребенку из тех, кто симпатичен им самим. За исключением тех детей, которым никто не симпатичен.
        /// </remarks>
        /// <returns>Имена детей</returns>
        public List<string> GetUnhappyChildren()
        {
            // ReSharper disable once ConvertIfStatementToNullCoalescingExpression Так понятнее
            if (_lastUnhappyChildrenResult == null)
            {
                _lastUnhappyChildrenResult = _realCalculator.GetUnhappyChildren();
            }

            return _lastUnhappyChildrenResult;
        }

        /// <summary>
        /// Возвращает список любимчиков
        /// </summary>
        /// <param name="count">
        /// Число любимчиков для поиска.
        /// </param>
        /// <remarks>
        /// Любимчики - детей, которые симпатичны максимальному количеству других детей.
        /// Если число запрошенных любимчиков больше либо равно числу детей, то будут выведены все дети по порядку убывания симпатий. За исключением числа не любимчиков.
        /// </remarks>
        /// <returns>Имена детей</returns>
        public List<string> GetFavoriteChildren(int count)
        {
            if (_lastFavoriteChildrenResult == null || _lastFavoriteChildrenResult.Count != count)
            {
                _lastFavoriteChildrenResult = _realCalculator.GetFavoriteChildren(count);
            }

            return _lastFavoriteChildrenResult;
        }
    }
}