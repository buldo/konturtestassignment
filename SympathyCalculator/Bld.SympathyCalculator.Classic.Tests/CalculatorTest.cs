namespace Bld.SympathyCalculator.Classic.Tests
{
    using Common.Tests;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class CalculatorTest : CalculatorTestBase
    {
        public CalculatorTest()
            : base(new ClassicCalculator())
        {
        }

        [TestMethod]
        public void TestNonFavorite()
        {
            TestNonFavoriteRealization();
        }

        [TestMethod]
        public void TestUnhappy()
        {
            TestUnhappyRealization();
        }

        [TestMethod]
        public void TestFavorite()
        {
            TestFavoriteRealization();
        }
    }
}
